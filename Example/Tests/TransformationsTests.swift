import XCTest
import InteractiveContainerView

class TransformationsTests: XCTestCase {

    let accuracy: CGFloat = 0.01

    func testAffineTransformComponents() {
        let size = CGSize(width: 300, height: 500)
        let center = CGPoint(x: 40, y: 80)
        let scale = 0.5
        let rotation = CGFloat.pi/3

        let transform = CGAffineTransform(
            scale: scale,
            rotation: rotation,
            viewSize: size,
            center: center
        )

        let components = transform.components(viewSize: size)

        XCTAssertEqual(scale, components.scale, accuracy: accuracy, "Scale should be correct")
        XCTAssertEqual(rotation, components.rotation, accuracy: accuracy, "Rotation should be correct")
        XCTAssertEqual(center.x, components.center.x, accuracy: accuracy, "Rotation should be correct")
        XCTAssertEqual(center.y, components.center.y, accuracy: accuracy, "Rotation should be correct")
     }
}

extension CGAffineTransform {
    init(
        scale: CGFloat,
        rotation: CGFloat,
        viewSize: CGSize,
        center: CGPoint
    ) {
        self = CGAffineTransform(
            translationX: viewSize.width/2 - center.x,
            y: viewSize.height/2 - center.y
        )
        .concatenating(CGAffineTransform(scaleX: scale, y: scale))
        .concatenating(CGAffineTransform(rotationAngle: rotation))
    }

    func components(viewSize: CGSize) -> (
        scale: CGFloat,
        rotation: CGFloat,
        translation: CGPoint,
        center: CGPoint
    ) {
        let scale = self.scale
        let rotation = self.rotation
        let translation = self.translation

        let invertedTranslation = translation.applying(
            CGAffineTransform(scaleX: scale, y: scale)
                .concatenating(CGAffineTransform(rotationAngle: rotation))
                .inverted()
        )

        let center = CGPoint(
            x: viewSize.width/2 - invertedTranslation.x,
            y: viewSize.height/2 - invertedTranslation.y
        )

        return (
            scale: scale,
            rotation: rotation,
            translation: translation,
            center: center
        )
    }
}
