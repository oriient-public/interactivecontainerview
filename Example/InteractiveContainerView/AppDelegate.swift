//
//  AppDelegate.swift
//  InteractiveContainerView
//
//  Created by Michael on 11/03/2020.
//  Copyright (c) 2020 Michael. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

}

