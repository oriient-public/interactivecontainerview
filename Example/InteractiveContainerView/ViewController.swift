//
//  ViewController.swift
//  InteractiveContainerView
//
//  Created by Michael on 11/03/2020.
//  Copyright (c) 2020 Michael. All rights reserved.
//

import UIKit
import InteractiveContainerView

class ViewController: UIViewController {

    @IBOutlet weak var interactiveContainerView: InteractiveContainerView!

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let image = UIImage(named: "map") else {
            fatalError("Missing image asset")
        }

        // we use simple CALayer with image but it can be any other layer
        // CAShapeLayer for shapes or maybe CATiledLayer for high-res tiled images
        let imageLayer = CALayer()
        imageLayer.contents = image.cgImage
        imageLayer.contentsGravity = .resizeAspect

        // The container bounds can be limited by the Safe Area
        // so we will allow content to be visible outside of it
        interactiveContainerView.clipsToBounds = false

        // we can add multiple layers, order managed as usually for CALayers
        interactiveContainerView.addContentLayer(imageLayer)

        // The content size is important to define how
        // `contentAspectScale`, `contentAspectOffset` calculated
        // and affects scroll limits
        interactiveContainerView.contentSize = image.size.screenPoints
    }

    // Converts point relative to the image to point relative to the content layer
    // The image on the content layer is scaled because of the screen resolution
    // and has offset because it's ratio is not the same as the screen ratio
    func convertPointOnImageToPointOnScreen(_ point: CGPoint) -> CGPoint {
        //these values are calculated automatically if `interactiveContainerView.contentSize` defined correctly
        let containerContentScale = self.interactiveContainerView.contentAspectScale
        let containerContentOffset = self.interactiveContainerView.contentAspectOffset

        // screen resolution applied
        var point = point.screenPoints

        // screen ratio - scale + offset
        point = point.applying(
            CGAffineTransform(
                scaleX: containerContentScale,
                y: containerContentScale
            )
        )
        point = point.applying(
            CGAffineTransform(
                translationX: containerContentOffset.x,
                y: containerContentOffset.y
            )
        )
        return point
    }

    @IBAction func testFocusAction(_ sender: Any) {
        let alert = UIAlertController(title: "Select location", message: nil, preferredStyle: .alert)

        // Coordinates in pixels relative to the image's top-left corner
        // will be converted to coordinates on the container using `convertPointOnImageToPointOnScreen`
        let locations: [(name: String, coordinates: CGPoint)] = [
            (name: "King's Landing", coordinates: CGPoint(x: 1302, y: 2906)),
            (name: "Winterfell", coordinates: CGPoint(x: 935, y: 1276)),
            (name: "Riverrun", coordinates: CGPoint(x: 851, y: 2483)),
            (name: "Highgarden", coordinates: CGPoint(x: 594, y: 3376)),
            (name: "Pyke", coordinates: CGPoint(x: 400, y: 2340)),
            (name: "Castle Black", coordinates: CGPoint(x: 1241, y: 660))
        ]

        locations.forEach { location in
            alert.addAction(
                UIAlertAction(
                    title: location.name,
                    style: .default,
                    handler: { _ in
                        self.interactiveContainerView.focus(
                            on: self.convertPointOnImageToPointOnScreen(location.coordinates),
                            zoomScale: 4,
                            rotation: self.interactiveContainerView.defaultContentRotation,
                            animated: true
                        )
                    }
                )
            )
        }

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
}

