# InteractiveContainerView

[![Version](https://img.shields.io/cocoapods/v/InteractiveContainerView.svg?style=flat)](https://cocoapods.org/pods/InteractiveContainerView)
[![License](https://img.shields.io/cocoapods/l/InteractiveContainerView.svg?style=flat)](https://cocoapods.org/pods/InteractiveContainerView)
[![Platform](https://img.shields.io/cocoapods/p/InteractiveContainerView.svg?style=flat)](https://cocoapods.org/pods/InteractiveContainerView)

Container view with zooming, scrolling and rotation support.

# Important: Zooming behaves quite unpredictably on simulators, please test using real devices.

## Overview

The container view tracks the movements of fingers and applies zooming, scrolling and rotation accordingly.

Main Features:

- Pan to scroll the content, natural decelerations is applied when the finger is raised;
- Scrolling is limited so content never leaves the visible part of the screen;
- Zoom and rotation around a pivot point between fingers;
- Focusing on a specific point on the screen;
- All transformtations events are reported to the delegate;
- Tap, double tap, pan, pinch, rotate, long-press gestures are handled and reported to the delegate so you can apply additional gestures-handling logic.

## Example

To run the simple usage example project, clone the repo, and run `pod install` from the Example directory first.
You can see how the container is used to present a map of Westeros and focus on important locations.

## Requirements

The InteractiveContainerView depends only on UIKit and supports iOS 10+

## Installation

InteractiveContainerView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'InteractiveContainerView'
```

## Usage 

InteractiveContainerView is a UIView subclass, can be created both programatically or using IB.

Setup the container and add some content layers

```swift
    // interactiveContainerView created the way you want

    // The content size is important to define how
    // `contentAspectScale`, `contentAspectOffset` calculated
    // and affects scroll limits
    interactiveContainerView.contentSize = image.size.screenPoints

    // Any CALayer can be used, for example CALayer with layer.contents = someCGImage
    // CAShapeLayer for shapes or maybe CATiledLayer for high-res tiled images
    let contentLayer = CALayer()

    // we can add multiple layers, order managed as usually for CALayers
    interactiveContainerView.addContentLayer(contentLayer)
    
    // to respond to the transformations
    interactiveContainerView.delegate = self
```

Respond to user-gestures:

```swift
// We can temporarily disable gesture handling to apply custom logic 
// For example: Drag some objects inside the content

func onContentDragged(to point: CGPoint, translation: CGPoint, state: UIGestureRecognizer.State) {
    switch state {
    case .began:
        // tell the container to stop gestures handling
        interactiveContainerView.isContentTransformationEnabled = false
    case .changed:
        // Use translation to drag some object
    case .ended, .cancelled, .failed:
        // recover gestures handling
        interactiveContainerView.isContentTransformationEnabled = true
    default: break
    }
}
```

## License

InteractiveContainerView is available under the Oriient New Media Ltd. license. See the LICENSE file for more info.
