
Pod::Spec.new do |s|
  s.name             = 'InteractiveContainerView'
  s.version          = '0.7.1'
  s.summary          = 'Container view with zooming, scrolling and rotation support.'
  s.description      = <<-DESC
  The container view tracks the movements of fingers and applies zooming, scrolling and rotation accordingly.
  The rotation and zooming respect gesture location (pivot point), scrolling supports deceleration and is limited so the content will never leave the visible area of the screen.
  The container has a delegate which allows you to respond to the content transformations events.
                       DESC
  s.homepage         = 'https://gitlab.com/oriient-public/interactivecontainerview'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Michael Krutoyarskiy' => 'michaelk@oriient.me' }
  s.source           = { :git => 'https://gitlab.com/oriient-public/interactivecontainerview.git', :tag => s.version.to_s }
  s.ios.deployment_target = '13.0'
  s.source_files = 'InteractiveContainerView/Classes/**/*'
  s.frameworks = 'UIKit'
  s.swift_version = '5.0'

end
