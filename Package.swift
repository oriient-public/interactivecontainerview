// swift-tools-version:5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "InteractiveContainerView",
    platforms: [.iOS(.v13)],
    products: [
        .library(
            name: "InteractiveContainerView",
            targets: [
                "InteractiveContainerView"
            ]
        ),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "InteractiveContainerView",
            path: "InteractiveContainerView/Classes"
        )
    ]
)
