//
//  InteractiveContainerViewDelegate.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 27/10/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// Allows to handle content transformations and user gestures
public protocol InteractiveContainerViewDelegate: AnyObject {
    /// Content transformed (as a result of user interaction, focus change or container bounds update)
    /// - Parameter newTransform: New content transform value
    func onContentTransformed(_ newTransform: CGAffineTransform)

    /// Content offset and scale inside container changed. (Due to content size or container bounds update)
    func onContentParametersChanged()

    /// User start touching the content
    func onUserInteractionStarted()

    /// User stopped touching the content
    func onUserInteractionEnded()

    /// Content tapped
    /// - Parameter point: Location of the gesture relative to the content coordinate system
    func onContentTapped(_ point: CGPoint)

    /// Content longpressed
    /// - Parameters:
    ///   - point: Location of the gesture relative to the content coordinate system
    ///   - state: State of the gesture
    func onContentLongpressed(_ point: CGPoint, state: UIGestureRecognizer.State)

    /// Content double tapped
    /// - Parameter point: Location of the gesture relative to the content coordinate system
    func onContentDoubleTapped(_ point: CGPoint)

    /// Content dragged
    /// - Parameters:
    ///   - point: Final location of the gesture relative to the content coordinate system
    ///   - translation: Translation since the last method call
    ///   - state: Gesture state
    func onContentDragged(to point: CGPoint, translation: CGPoint, state: UIGestureRecognizer.State)

    /// Content pinched
    /// - Parameters:
    ///   - scaleDelta: Scale since the last method call
    ///   - point: Location of the point between fingers relative to the content coordinate system
    ///   - state: Gesture state
    func onContentPinched(scaleDelta: CGFloat, around point: CGPoint, state: UIGestureRecognizer.State)

    /// Content rotated
    /// - Parameters:
    ///   - angleDelta: Rotation (radians) since the last method call
    ///   - point: Location of the point between fingers relative to the content coordinate system
    ///   - state: Gesture state
    func onContentRotated(angleDelta: CGFloat, around point: CGPoint, state: UIGestureRecognizer.State)
}
