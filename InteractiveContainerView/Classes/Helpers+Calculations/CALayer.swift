//
//  CALayer.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 27/10/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

public extension CALayer {
    /// Adds simple fade animation, removed on completion
    func addFadeAnimation(duration: Double = 0.3) {
        let animation = CATransition()
        animation.type = CATransitionType.fade
        animation.duration = duration
        animation.isRemovedOnCompletion = true
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        self.add(animation, forKey: nil)
    }

    /// Executes block inside a CATransaction
    static func wrapInCAAnimationTransaction(
        _ block: () -> Void,
        duration: Double = 0.5,
        mediaFunction: CAMediaTimingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    ) {
        CATransaction.begin()
        CATransaction.setAnimationDuration(duration)
        CATransaction.setAnimationTimingFunction(mediaFunction)

        block()

        CATransaction.commit()
    }

    /// Disables implicit animations inside a CATransaction
    static func disableImplicitAnimations(_ block: () -> Void) {
        CATransaction.begin()
        CATransaction.setDisableActions(true)

        block()

        CATransaction.commit()
    }

    /// Disables implicit animations for a specific property
    func disableImplicitActions(for property: String) {
        self.actions = [property: NSNull()]
    }

    /// Disables implicit animations for `transform` property
    func disableImplicitTransfromAnimation() {
        disableImplicitActions(for: "transform")
    }

    /// Disables implicit animations for `position` property
    func disableImplicitPositionAnimation() {
        disableImplicitActions(for: "position")
    }
}
