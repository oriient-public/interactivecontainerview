//
//  SegmentedLine.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 27/10/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

/// Represents a segmented line between two points
public struct SegmentedLine {
    public let a: CGPoint
    public let b: CGPoint


    public init(a: CGPoint, b: CGPoint) {
        self.a = a
        self.b = b
    }
}

public extension SegmentedLine {
    /// Calculates minumum distance to a segmented line
   func distance(to line: SegmentedLine) -> CGFloat {
        if Self.intersectionOfLines(line1: self, line2: line) != nil {
            return 0
        }
        return min(
            self.a.distance(to: line),
            self.b.distance(to: line),
            line.a.distance(to: self),
            line.b.distance(to: self)
        )
    }

    /// Finds an intersection point of two segmented lines (if there is one)
    static func intersectionOfLines(line1: SegmentedLine, line2: SegmentedLine) -> CGPoint? {
        // parallel lines
        let distance = (line1.b.x - line1.a.x) * (line2.b.y - line2.a.y) - (line1.b.y - line1.a.y) * (line2.b.x - line2.a.x)
        if distance == 0 {
            return nil
        }

        let u = ((line2.a.x - line1.a.x) * (line2.b.y - line2.a.y) - (line2.a.y - line1.a.y) * (line2.b.x - line2.a.x)) / distance
        let v = ((line2.a.x - line1.a.x) * (line1.b.y - line1.a.y) - (line2.a.y - line1.a.y) * (line1.b.x - line1.a.x)) / distance

        if (u < 0.0 || u > 1.0) {
            // intersection not inside line1
            return nil
        }
        if (v < 0.0 || v > 1.0) {
            // intersection not inside line2
            return nil
        }

        return CGPoint(
            x: line1.a.x + u * (line1.b.x - line1.a.x),
            y: line1.a.y + u * (line1.b.y - line1.a.y)
        )
    }

    /// Calculates minimum offset it is necessary to apply to the line1 so the lines will intersect
    static func offsetBetweenLines(line1: SegmentedLine, line2: SegmentedLine) -> CGPoint {
        [
            line1.a.offset(from: line2),
            line1.b.offset(from: line2),
            line2.a.offset(from: line1).inverted,
            line2.b.offset(from: line1).inverted
        ].sorted(by: { $0.length < $1.length })[0]
    }

    static func offsetsBetweenLines(line1: SegmentedLine, line2: SegmentedLine) -> [CGPoint] {
        [
            line1.a.offset(from: line2),
            line1.b.offset(from: line2),
            line2.a.offset(from: line1).inverted,
            line2.b.offset(from: line1).inverted
        ]
    }
}
