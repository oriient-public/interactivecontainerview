//
//  CGSize.swift
//  IPSMapView
//
//  Created by Michael Krutoyarskiy on 27/10/2020.
//  Copyright © 2020 Michael Krutoyarskiy. All rights reserved.
//

import Foundation
import UIKit

public extension CGSize {
    /// Pixels devided by `UIScreen.main.scale`
    var screenPoints: CGSize { CGSize(width: width/UIScreen.main.scale, height: height/UIScreen.main.scale) }
}
